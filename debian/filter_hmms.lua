#!/usr/bin/env lua

function usage()
  io.stderr:write(string.format("Usage: %s <filter> [<invert>] < file\n", arg[0]))
  io.stderr:write('Filters out a HMM with NAME matching <filter>.\n')
  os.exit(1)
end

if #arg < 1 then
  usage()
end

lastname = nil
lines = {}
for l in io.lines() do
  name = l:match('NAME%s+(.*)')
  table.insert(lines, l)
  if name then
    lastname = name
  elseif l:match('//') then
    if arg[2] then
      if lastname:match(arg[1]) then
        print(table.concat(lines, '\n'))
      end
    else
      if not lastname:match(arg[1]) then
        print(table.concat(lines, '\n'))
      end
    end
    lines = {}
  end
end



